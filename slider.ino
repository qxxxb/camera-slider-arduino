#include <AccelStepper.h>

#ifdef DEBUG
  #define DEBUG_PRINT(x) Serial.print (x)
  #define DEBUG_PRINTLN(x) Serial.println (x)
#else
  #define DEBUG_PRINT(x)
  #define DEBUG_PRINTLN(x)
#endif

enum Command {
  cmdPinMode,
  cmdDigitalWrite,
  cmdAnalogWrite,
  cmdMoveTo,
  cmdSetAcceleration,
  cmdSetMaxSpeed,
  cmdSetSpeed,
  cmdDistanceToGo,
  cmdTargetPosition,
  cmdCurrentPosition,
  cmdHasArrived
};

AccelStepper stepper(
  AccelStepper::HALF4WIRE,
  2, 4, 7, 8
);

const int pwmA = 3;
const int pwmB = 5;

// The current duty cycle for `pwmA` and pwmB`. Should be 0 when motor is not
// running.
int dutyCycle;

// Whether to use accelerations or use a constant speed
bool useAccelerations = 0;

// Delay in milliseconds between calls to `Serial.available()`
const int serialDelay = 100;

// Time in milliseconds of the last serial query
unsigned long lastSerialQuery;

void setup() {
  Serial.begin(9600);

  stepper.setMaxSpeed(400.0);

  pinMode(pwmA, OUTPUT);
  pinMode(pwmB, OUTPUT);
  digitalWrite(pwmA, LOW);
  digitalWrite(pwmB, LOW);
}

void loop() {
  if (((millis() - lastSerialQuery) / serialDelay > 0) && Serial.available()) {
    lastSerialQuery = millis();
    Command command = (Command)Serial.read();

    switch (command) {
      case cmdPinMode: {
        byte bytes[4];
        Serial.readBytes(bytes, 2);
        int pin = *((int*)bytes);

        Serial.readBytes(bytes, 2);
        int mode = *((int*)bytes);

        DEBUG_PRINT("Pin mode: ");
        DEBUG_PRINT(pin);
        DEBUG_PRINT(", ");

        switch (mode) {
          case 0:
            pinMode(pin, OUTPUT);
            DEBUG_PRINTLN("OUTPUT");
            break;
          case 1:
            pinMode(pin, INPUT);
            DEBUG_PRINTLN("INPUT");
            break;
          case 2:
            pinMode(pin, INPUT_PULLUP);
            DEBUG_PRINTLN("INPUT_PULLUP");
            break;
        }

        break;

      } case cmdDigitalWrite: {
        byte bytes[4];
        Serial.readBytes(bytes, 2);
        int pin = *((int*)bytes);

        Serial.readBytes(bytes, 1);
        bool value = *((bool*)bytes);

        DEBUG_PRINT("Digital write: ");
        DEBUG_PRINT(pin);
        DEBUG_PRINT(", ");

        if (value) {
          digitalWrite(pin, HIGH);
          DEBUG_PRINTLN("HIGH");
        } else {
          digitalWrite(pin, LOW);
          DEBUG_PRINTLN("LOW");
        }

        break;

      } case cmdAnalogWrite: {
        byte bytes[4];
        Serial.readBytes(bytes, 2);
        int pin = *((int*)bytes);

        Serial.readBytes(bytes, 2);
        int value = *((int*)bytes);

        analogWrite(pin, value);

        DEBUG_PRINT("Analog write: ");
        DEBUG_PRINT(pin);
        DEBUG_PRINT(", ");
        DEBUG_PRINTLN(value);

        break;

      } case cmdMoveTo: {
        byte bytes[4];
        Serial.readBytes(bytes, 4);
        long targetPos = *((long*)bytes);
        stepper.moveTo(targetPos);

        DEBUG_PRINT("Target pos: ");
        DEBUG_PRINTLN(targetPos);

        break;

      } case cmdSetAcceleration: {
        byte bytes[4];
        Serial.readBytes(bytes, 4);
        float acceleration = *((float*)bytes);
        stepper.setAcceleration(acceleration);

        DEBUG_PRINT("Acceleration: ");
        DEBUG_PRINTLN(acceleration);

        useAccelerations = 1;

        break;

      } case cmdSetMaxSpeed: {
        byte bytes[4];
        Serial.readBytes(bytes, 4);
        float speed = *((float*)bytes);
        stepper.setMaxSpeed(speed);

        DEBUG_PRINT("Max speed: ");
        DEBUG_PRINTLN(speed);

        break;

      } case cmdSetSpeed: {
        byte bytes[4];
        Serial.readBytes(bytes, 4);
        float speed = *((float*)bytes);
        stepper.setSpeed(speed);

        DEBUG_PRINT("Speed: ");
        DEBUG_PRINTLN(speed);

        useAccelerations = 0;

        break;

      } case cmdDistanceToGo: {
        union {
          long val;
          byte bytes[4];
        } pos;

        pos.val = stepper.distanceToGo();
        Serial.write(pos.bytes, 4);

        break;

      } case cmdTargetPosition: {
        union {
          long val;
          byte bytes[4];
        } pos;

        pos.val = stepper.targetPosition();
        Serial.write(pos.bytes, 4);

        break;

      } case cmdCurrentPosition: {
        union {
          long val;
          byte bytes[4];
        } pos;

        pos.val = stepper.currentPosition();
        Serial.write(pos.bytes, 4);

        break;

      } case cmdHasArrived: {
        Serial.write(stepper.distanceToGo() == 0);
        break;

      } default: {
        DEBUG_PRINT("Unknown command: ");
        DEBUG_PRINTLN(command);
        break;
      }
    }
  }

  if (useAccelerations) {
    stepper.run();
  } else {
    stepper.runSpeedToPosition();
  }

  if (stepper.distanceToGo() == 0 && dutyCycle > 0) {
    dutyCycle = 0;
    digitalWrite(pwmA, LOW);
    digitalWrite(pwmB, LOW);
  } else {
    float speed = abs(stepper.speed());
    if (speed == 0.0f) {
      dutyCycle = 0;
    } else if (speed <= 100.0f) {
      dutyCycle = 48;
    } else {
      dutyCycle = 80;
    }

    analogWrite(pwmA, dutyCycle);
    analogWrite(pwmB, dutyCycle);
  }
}
